#include "MainWindow.h"
#include "GeneratedCodeWindow.h"

MainWindow::MainWindow(): headerText_("") {
    setWindowTitle("Header Generator");
    QGridLayout *mainLayout = new QGridLayout;

    //FIRST MAIN LAYOUT
    //
    //Definition Box
    QGroupBox *defGroupBox = new QGroupBox("Class Definition");
    QFormLayout *defFormLayout = new QFormLayout;

    name = new QLineEdit;
    parentClass = new QLineEdit;

    defFormLayout->addRow("&Name : ", name);
    defFormLayout->addRow("&Parent Class : ", parentClass);
    defGroupBox->setLayout(defFormLayout);

    mainLayout->addWidget(defGroupBox, 0, 0);


    //Options Box
    QGroupBox *optionsGroupBox = new QGroupBox("Options");
    QVBoxLayout *optionsVBox = new QVBoxLayout;

    optionsCheck1 = new QCheckBox("Protect &header against multiple inclusions");
    optionsCheck2 = new QCheckBox("Generate a default &constructor");
    optionsCheck3 = new QCheckBox("Generate a &destructor");
    optionsCheck4 = new QCheckBox("Generate &Getters && Setters");

    optionsVBox->addWidget(optionsCheck1);
    optionsVBox->addWidget(optionsCheck2);
    optionsVBox->addWidget(optionsCheck3);
    optionsVBox->addWidget(optionsCheck4);
    optionsGroupBox->setLayout(optionsVBox);

    connect(optionsCheck1, SIGNAL(stateChanged(int)), this, SLOT(defHeaderDis()));

    mainLayout->addWidget(optionsGroupBox, 1, 0);


    //Commentaries Box
    QFormLayout *commentLayout = new QFormLayout;
    commentGroupBox = new QGroupBox("Add Co&mmentaries");

    author = new QLineEdit;
    QDateTime *qDate = new QDateTime(QDateTime::currentDateTime());
    creaDate = new QDateEdit(qDate->date());
    classRole = new QTextEdit;

    commentGroupBox->setCheckable(true);
    commentGroupBox->setChecked(false);
    commentLayout->addRow("&Author :", author);
    commentLayout->addRow("Crea&tion Date : ", creaDate);
    commentLayout->addRow("Class &Role : ", classRole);
    commentGroupBox->setLayout(commentLayout);

    mainLayout->addWidget(commentGroupBox, 2, 0);

    //Buttons
    QHBoxLayout *buttonsLayout = new QHBoxLayout;
    buttonsLayout->setDirection(QBoxLayout::RightToLeft);

    generateButton = new QPushButton("Generate !");
    exitButton = new QPushButton("Exit");

    buttonsLayout->addWidget(exitButton);
    buttonsLayout->addWidget(generateButton);

    mainLayout->addLayout(buttonsLayout, 3, 0);

    //END FIRST MAIN LAYOUT

    //SECOND MAIN LAYOUT
    //
    //Includes Group
    QGroupBox *includesGroupBox = new QGroupBox("Includes");
    QFormLayout *includesLayout = new QFormLayout;
    QVBoxLayout *includesMainLayout = new QVBoxLayout;
    QHBoxLayout *includesMainButtonsLayout = new QHBoxLayout;

    QPushButton *includesButton = new QPushButton("Add");
    QPushButton *includesMainHisButton = new QPushButton("History");
    QPushButton *includesEditButton = new QPushButton("Edit");

    includesNumber = new QSpinBox;
    includesNumber->setMinimum(1);

    includesMainButtonsLayout->addWidget(includesButton);
    includesMainButtonsLayout->addWidget(includesEditButton);
    includesMainButtonsLayout->addWidget(includesMainHisButton);

    includesLayout->addRow("Includes Number :", includesNumber);
    includesMainLayout->addLayout(includesLayout);
    includesMainLayout->addLayout(includesMainButtonsLayout);
    includesGroupBox->setLayout(includesMainLayout);

    mainLayout->addWidget(includesGroupBox, 0, 1);
    
    connect(includesEditButton, SIGNAL(clicked()), this, SLOT(editIncludes()));
    connect(includesMainHisButton, SIGNAL(clicked()), this, SLOT(includesHistory()));
    connect(includesButton, SIGNAL(clicked()), this, SLOT(displayIncludesAdd()));


    //Attributes Group
    QGroupBox *attributesGroupBox = new QGroupBox("Attributes And Methods");
    QVBoxLayout *attributesMainLayout = new QVBoxLayout;
    QHBoxLayout *radioLayout = new QHBoxLayout;
    QHBoxLayout *secondRadioLayout = new QHBoxLayout;
    QHBoxLayout *attributesButtonsLayout = new QHBoxLayout;
    QButtonGroup *secondRadioGroup = new QButtonGroup;

    publicRadio = new QRadioButton("Public");
    protectedRadio = new QRadioButton("Protected");
    privateRadio = new QRadioButton("Private");

    attributesSpinBox = new QSpinBox;
    attributesSpinBox->setMinimum(1);

    attributesRadio = new QRadioButton("Attributes");
    methodsRadio = new QRadioButton("Methods");

    QPushButton *attributesAddButton = new QPushButton("Add");
    QPushButton *attributesEditButton = new QPushButton("Edit");
    QPushButton *attributesHistoryButton = new QPushButton("History");

    radioLayout->addWidget(publicRadio);
    radioLayout->addWidget(protectedRadio);
    radioLayout->addWidget(privateRadio);

    secondRadioGroup->addButton(attributesRadio);
    secondRadioGroup->addButton(methodsRadio);
    secondRadioLayout->addWidget(attributesRadio);
    secondRadioLayout->addWidget(methodsRadio);

    attributesButtonsLayout->addWidget(attributesAddButton);
    attributesButtonsLayout->addWidget(attributesEditButton);
    attributesButtonsLayout->addWidget(attributesHistoryButton);

    attributesMainLayout->addLayout(radioLayout);
    attributesMainLayout->addWidget(attributesSpinBox);
    attributesMainLayout->addLayout(secondRadioLayout);
    attributesMainLayout->addLayout(attributesButtonsLayout);
    
    attributesGroupBox->setLayout(attributesMainLayout);
    mainLayout->addWidget(attributesGroupBox, 1, 1);

    connect(attributesAddButton, SIGNAL(clicked()), this, SLOT(attribsAndMethodsCheck()));
    connect(attributesEditButton, SIGNAL(clicked()), this, SLOT(attributesEdit()));
    connect(attributesHistoryButton, SIGNAL(clicked()), this, SLOT(attributesHistory()));


    //END SECOND MAIN LAYOUT

    //Main Layout
    setLayout(mainLayout);
     
    
    connect(exitButton, SIGNAL(clicked()), qApp, SLOT(quit()));
    connect(generateButton, SIGNAL(clicked()), this, SLOT(generateCode()));
}

void MainWindow::generateCode() {
    if (name->text().isEmpty()) {
        QMessageBox::critical(this, "Error", "You must at lease enter a class name !");
        return;

    }
    QString code("");
    QString codeCpp("");

    codeCpp += "#include \"" + name->text() + ".h\"\n\n";

    if (headerText_.isEmpty()) {
        headerText_ = "DEF_" + name->text().toUpper();
    }

    if (commentGroupBox->isChecked()) {
        code += "/*\n";

        if (!author->text().isEmpty()) {
            code += "Author : " + author->text() + "\n";

        }
        code += "Creation Date : " + creaDate->date().toString("dd / MM / yyyy") + "\n\n";

        if (!classRole->toPlainText().isEmpty()) {
            code += "Role : \n" + classRole->toPlainText();

        }
        code += "\n*/\n\n";
    }

    if (optionsCheck1->isChecked()) {
        code += "#ifndef " + headerText_ + "\n#define " + headerText_ + "\n\n";

    }
    for (int i = 0; i < includesList_.size(); i++) {
        code += "#include " + includesList_[i] + "\n";
        if (i == includesList_.size() - 1) {
            code += "\n";

        }
    }
    code += "class " + name->text();

    if (!parentClass->text().isEmpty()) {
        code += ": public " + parentClass->text();

    }
    code += " {\n    public:";

    if (optionsCheck2->isChecked()) {
        code += "\n        " + name->text() + "();";
        codeCpp += name->text() + "::" + name->text() + "() {\n}\n\n";

    }
    if (optionsCheck3->isChecked()) {
        code += "\n        ~" + name->text() + "();";
        codeCpp += name->text() + "::~" + name->text() + "() {\n}\n\n";

    }
    if (optionsCheck4->isChecked()) {
        code += "\n";
        for (int i = 0; i < publicAttributesList_.size(); i++) {
            QString tempType(publicAttributesList_[i].split(" ").at(0));
            QString tempName(publicAttributesList_[i].split(" ").value(1));
            QString tempLowerName(tempName);
            tempName[0] = tempName[0].toUpper();

            code += "\n        " + tempType + " get" + tempName + "() const;\n        void set" + tempName + "(" + tempType + " new" + tempName + ");";
            codeCpp += tempType + " " + name->text() + "::" + "get" + tempName + "() const {\n    return " + tempLowerName + ";\n}\n\n";
            codeCpp += "void " + name->text() + "::" + "set" + tempName + "(" + tempType + " new" + tempName + ") {\n    " + tempLowerName + " = new" + tempName + ";\n}\n\n";
        }
        for (int i = 0; i < protectedAttributesList_.size(); i++) {
            QString tempType(protectedAttributesList_[i].split(" ").at(0));
            QString tempName(protectedAttributesList_[i].split(" ").value(1));
            QString tempLowerName(tempName);
            tempName[0] = tempName[0].toUpper();

            code += "\n        " + tempType + " get" + tempName + "() const;\n        void set" + tempName + "(" + tempType + " new" + tempName + ");";
            codeCpp += tempType + " " + name->text() + "::" + "get" + tempName + "() const {\n    return " + tempLowerName + ";\n}\n\n";
            codeCpp += "void " + name->text() + "::" + "set" + tempName + "(" + tempType + " new" + tempName + ") {\n    " + tempLowerName + " = new" + tempName + ";\n}\n\n";
        }
        for (int i = 0; i < privateAttributesList_.size(); i++) {
            QString tempType(privateAttributesList_[i].split(" ").at(0));
            QString tempName(privateAttributesList_[i].split(" ").value(1));
            QString tempLowerName(tempName);
            tempName[0] = tempName[0].toUpper();

            code += "\n        " + tempType + " get" + tempName + "() const;\n        void set" + tempName + "(" + tempType + " new" + tempName + ");";
            codeCpp += tempType + " " + name->text() + "::" + "get" + tempName + "() const {\n    return " + tempLowerName + ";\n}\n\n";
            codeCpp += "void " + name->text() + "::" + "set" + tempName + "(" + tempType + " new" + tempName + ") {\n    " + tempLowerName + " = new" + tempName + ";\n}\n\n";
        }
    }
    if (publicMethodsList_.size() > 0) {
        code += "\n";

        for (int i = 0; i < publicMethodsList_.size(); i++) {
            QString tempQString;
            tempQString = publicMethodsList_[i]->value(0);
            code += "\n        " + tempQString + "(";
            codeCpp += publicMethodsList_[i]->value(0).split(" ").at(0) + " " + name->text() + "::" + tempQString.split(" ").value(1) + "(";

            for (int j = 1; j < publicMethodsList_[i]->size(); j++) {
                code += publicMethodsList_[i]->value(j);
                codeCpp += publicMethodsList_[i]->value(j);

                if (j < publicMethodsList_[i]->size() - 1) {
                    code += ", ";
                    codeCpp += ", ";

                }
            }
            code += ");";
            codeCpp +=") {\n\n}\n\n";
        }
    }
    if (publicAttributesList_.size() > 0) {
        code += "\n";

        for (int i = 0; i < publicAttributesList_.size(); i++) {
            code += "\n        " + publicAttributesList_[i] + ";";

        }
    }
    code += "\n\n    protected:";

    if (protectedMethodsList_.size() > 0) {
        code += "\n";

        for (int i = 0; i < protectedMethodsList_.size(); i++) {
            QString tempQString;
            tempQString = protectedMethodsList_[i]->value(0);
            code += "\n        " + tempQString + "(";
            codeCpp += protectedMethodsList_[i]->value(0).split(" ").at(0) + " " + name->text() + "::" + tempQString.split(" ").value(1) + "(";

            for (int j = 1; j < protectedMethodsList_[i]->size(); j++) {
                code += protectedMethodsList_[i]->value(j);
                codeCpp += protectedMethodsList_[i]->value(j);

                if (j < protectedMethodsList_[i]->size() - 1) {
                    code += ", ";
                    codeCpp += ", ";

                }
            }
            code += ");";
            codeCpp +=") {\n\n}\n\n";
        }
    }
    if (protectedAttributesList_.size() > 0) {
        code += "\n";

        for (int i = 0; i < protectedAttributesList_.size(); i++) {
            code += "\n        " + protectedAttributesList_[i] + ";";

        }
    }
    code += "\n\n    private:";

    if (privateMethodsList_.size() > 0) {
        code += "\n";

        for (int i = 0; i < privateMethodsList_.size(); i++) {
            QString tempQString;
            tempQString = privateMethodsList_[i]->value(0);
            code += "\n        " + tempQString + "(";
            codeCpp += privateMethodsList_[i]->value(0).split(" ").at(0) + " " + name->text() + "::" + tempQString.split(" ").value(1) + "(";

            for (int j = 1; j < privateMethodsList_[i]->size(); j++) {
                code += privateMethodsList_[i]->value(j);
                codeCpp += privateMethodsList_[i]->value(j);

                if (j < privateMethodsList_[i]->size() - 1) {
                    code += ", ";
                    codeCpp += ", ";

                }
            }
            code += ");";
            codeCpp +=") {\n\n}\n\n";
        }
    }
    if (privateAttributesList_.size() > 0) {
        code += "\n";

        for (int i = 0; i < privateAttributesList_.size(); i++) {
            code += "\n        " + privateAttributesList_[i] + ";";

        }
    }
    code += "\n\n};";

    if (optionsCheck1->isChecked()) {
        code += "\n\n#endif";

    }

    GeneratedCodeWindow *generatedCodeWin = new GeneratedCodeWindow(code, codeCpp, name->text());
}

void MainWindow::defHeaderDis() {
    if (optionsCheck1->isChecked()) {
        QDialog *defHeaderWin = new QDialog(this);
        QVBoxLayout *defHeaderLayout = new QVBoxLayout;
        defHeaderLine = new QLineEdit("DEF_" + name->text().toUpper());
        QPushButton *defHeaderChangeButton = new QPushButton("Change");
        
        defHeaderWin->setWindowTitle("Define");
        defHeaderLayout->addWidget(defHeaderLine);
        defHeaderLayout->addWidget(defHeaderChangeButton);

        defHeaderWin->setLayout(defHeaderLayout);

        connect(defHeaderChangeButton, SIGNAL(clicked()), this, SLOT(changeHead()));
        connect(defHeaderChangeButton, SIGNAL(clicked()), defHeaderWin, SLOT(close()));

        defHeaderWin->exec();
    }
}

void MainWindow::changeHead() {
    headerText_ = defHeaderLine->text();
}

void MainWindow::displayIncludesAdd() {
    index_ = 0;

    while (index_ < includesNumber->value()) {
        index_++;

        addIncludesWin = new QDialog;
        addIncludesWin->setWindowTitle("Add Includes");

        QVBoxLayout *addIncludesLayout = new QVBoxLayout;
        QHBoxLayout *addIncludesButtonsLayout = new QHBoxLayout;
        QLabel *includesLabel = new QLabel("Type the include you want to add (example : type '<stdio.h>' to add '#include <stdio.h>')");
        includesLineEdit = new QLineEdit;

        QPushButton *includesAcceptButton = new QPushButton("Confirm");
        QPushButton *includesListButton = new QPushButton("History");
        QPushButton *includesCancelButton = new QPushButton("Cancel");

        addIncludesButtonsLayout->addWidget(includesAcceptButton);
        addIncludesButtonsLayout->addWidget(includesListButton);
        addIncludesButtonsLayout->addWidget(includesCancelButton);

        addIncludesLayout->addWidget(includesLabel);
        addIncludesLayout->addWidget(includesLineEdit);
        addIncludesLayout->addLayout(addIncludesButtonsLayout);
        addIncludesWin->setLayout(addIncludesLayout);
        

        connect(includesListButton, SIGNAL(clicked()), this, SLOT(includesHistory()));
        connect(includesAcceptButton, SIGNAL(clicked()), this, SLOT(acceptInclude()));
        connect(includesAcceptButton, SIGNAL(clicked()), addIncludesWin, SLOT(close()));
        connect(includesCancelButton, SIGNAL(clicked()), this, SLOT(resetIndex()));
        connect(includesCancelButton, SIGNAL(clicked()), addIncludesWin, SLOT(close()));

        addIncludesWin->exec();
    }
}

void MainWindow::resetIndex() {
    index_ = 999999;
}

void MainWindow::resetIndex2() {
    index2_ = 999999;
}

void MainWindow::acceptInclude() {
    includesList_.append(includesLineEdit->text());
}

void MainWindow::includesHistory() {
    if (includesList_.size() == 0) {
        QMessageBox::information(this, "No Includes", "You don't have any includes yet.");
        return;
    }

    QDialog *historyWindow = new QDialog(addIncludesWin);
    QVBoxLayout *historyLayout = new QVBoxLayout;
    QPushButton *historyExitButton = new QPushButton("Back");

    for (int i = 0; i < includesList_.size(); i++) {
        QString index = QString::number(i + 1);
        historyLayout->addWidget(new QLabel(index + " - #include " + includesList_[i]));
    }
    historyLayout->addWidget(historyExitButton);

    connect(historyExitButton, SIGNAL(clicked()), historyWindow, SLOT(close()));

    historyWindow->setLayout(historyLayout);
    historyWindow->exec();
}

void MainWindow::editIncludes() {
    modifyButtonsVector_.clear();
    buttonsVector_.clear();

    if (includesList_.size() == 0) {
        QMessageBox::information(this, "No Includes", "You have no includes to edit yet.");
        return;
    }

    editIncludesWin = new QDialog;
    editIncludesWin->setWindowTitle("Edit Includes");
    QVBoxLayout *editIncludesLayout = new QVBoxLayout;

    for (int i = 0; i < includesList_.size(); i++) {

        QHBoxLayout *includesListHLayout = new QHBoxLayout;
        QLabel *editIncludesLabel = new QLabel(QString::number(i + 1) + " - #include " + includesList_[i]);
        QPushButton *editIncludesDeleteButton = new QPushButton("Delete");
        QPushButton *editIncludesModifyButton = new QPushButton("Modify");
        buttonsVector_.append(editIncludesDeleteButton);
        modifyButtonsVector_.append(editIncludesModifyButton);

        connect(editIncludesDeleteButton, SIGNAL(clicked()), this, SLOT(deleteInclude()));
        connect(editIncludesModifyButton, SIGNAL(clicked()), this, SLOT(modifyInclude()));

        includesListHLayout->addWidget(editIncludesLabel);
        includesListHLayout->addWidget(editIncludesDeleteButton);
        includesListHLayout->addWidget(editIncludesModifyButton);
        editIncludesLayout->addLayout(includesListHLayout);

    }
    QPushButton *editIncludesExitButton = new QPushButton("Back");
    editIncludesLayout->addWidget(editIncludesExitButton);

    connect(editIncludesExitButton, SIGNAL(clicked()), editIncludesWin, SLOT(close()));

    editIncludesWin->setLayout(editIncludesLayout);
    editIncludesWin->exec();
}

void MainWindow::deleteInclude() {
    QObject *obj = sender();
    
    for (int i = 0; i < buttonsVector_.size(); i++) {
        if (obj == buttonsVector_[i]) {
            includesList_.remove(i);
            buttonsVector_.clear();

            editIncludesWin->close();
            editIncludes();

            return;
        }
    }
}

void MainWindow::modifyInclude() {
    QObject *obj = sender();

    for (int i = 0; i < modifyButtonsVector_.size(); i++) {
        if (obj == modifyButtonsVector_[i]) {
            index_ = i;
            modifyIncludeWin = new QDialog;
            modifyIncludeWin->setWindowTitle("Modify Include");

            QVBoxLayout *modifyIncludeVLayout = new QVBoxLayout;
            QHBoxLayout *modifyIncludeHLayout = new QHBoxLayout;

            modifyIncludeLineEdit = new QLineEdit(includesList_[i]);
            QPushButton *modifyIncludeAcceptButton = new QPushButton("Modify");
            QPushButton *modifyIncludeCancelButton = new QPushButton("Cancel");

            modifyIncludeHLayout->addWidget(modifyIncludeAcceptButton);
            modifyIncludeHLayout->addWidget(modifyIncludeCancelButton);
            modifyIncludeVLayout->addWidget(modifyIncludeLineEdit);
            modifyIncludeVLayout->addLayout(modifyIncludeHLayout);
            modifyIncludeWin->setLayout(modifyIncludeVLayout);

            connect(modifyIncludeAcceptButton, SIGNAL(clicked()), this, SLOT(acceptIncludeModification()));
            connect(modifyIncludeAcceptButton, SIGNAL(clicked()), modifyIncludeWin, SLOT(close()));
            connect(modifyIncludeCancelButton, SIGNAL(clicked()), modifyIncludeWin, SLOT(close()));

            modifyIncludeWin->exec();
            editIncludes();
            return;
        }
    }
}

void MainWindow::acceptIncludeModification() {
    editIncludesWin->close();
    includesList_[index_] = modifyIncludeLineEdit->text();
}

void MainWindow::attribsAndMethodsCheck() {
    if (!publicRadio->isChecked() && !protectedRadio->isChecked() && !privateRadio->isChecked()) {
        QMessageBox::critical(this, "Type Error", "You must choose a type !");
        return;
    }
    if (!attributesRadio->isChecked() && !methodsRadio->isChecked()) {
        QMessageBox::critical(this, "Error", "You need to specify if it's an attribute or a method !");
        return;
    }

    if (attributesRadio->isChecked()) {
        attributesAdd();

    } else {
        methodsAdd();

    }
}

void MainWindow::methodsAdd() {
    index_ = 0;

    while (index_ < attributesSpinBox->value()) {
        index_++;
        methodsAddWin = new QDialog;
        methodsAddWin->setWindowTitle("Add Method");

        QVBoxLayout *methodsAddLayout = new QVBoxLayout;
        QHBoxLayout *methodsAddButtonsLayout = new QHBoxLayout;
        
        QLabel *methodsAddLabel = new QLabel("Type the method you want to add and choose the number of parameters. (Example : \"void helloWorld\")");
        methodsLineEdit = new QLineEdit;
        QPushButton *methodsAddAcceptButton = new QPushButton("Confirm");
        QPushButton *methodsAddCancelButton = new QPushButton("Cancel");
        QPushButton *methodsAddHistoryButton = new QPushButton("History");
        methodsAddSpinBox = new QSpinBox;
        
        methodsAddButtonsLayout->addWidget(methodsAddAcceptButton);
        methodsAddButtonsLayout->addWidget(methodsAddCancelButton);
        methodsAddButtonsLayout->addWidget(methodsAddHistoryButton);
        methodsAddLayout->addWidget(methodsAddLabel);
        methodsAddLayout->addWidget(methodsLineEdit);
        methodsAddLayout->addWidget(methodsAddSpinBox);
        methodsAddLayout->addLayout(methodsAddButtonsLayout);

        connect(methodsAddAcceptButton, SIGNAL(clicked()), this, SLOT(methodsAddParameters()));
        connect(methodsAddHistoryButton, SIGNAL(clicked()), this, SLOT(attributesHistory()));
        connect(methodsAddCancelButton, SIGNAL(clicked()), this, SLOT(resetIndex()));
        connect(methodsAddCancelButton, SIGNAL(clicked()), methodsAddWin, SLOT(close()));

        methodsAddWin->setLayout(methodsAddLayout);
        methodsAddWin->exec();
    }
}

void MainWindow::methodsAddParameters() {
    index2_ = 0;
    tempParameterVector_.clear();

    if (methodsAddSpinBox->value() == 0) {
        methodsAddConfirm();
        return;
    }

    while (index2_ < methodsAddSpinBox->value()) {
        index2_++;

        methodsAddParameterWin = new QDialog;
        methodsAddParameterWin->setWindowTitle("Add Parameter");

        QVBoxLayout *methodsAddParameterLayout = new QVBoxLayout;
        QHBoxLayout *methodsAddParameterButtonsLayout = new QHBoxLayout;

        methodsAddParameterLineEdit = new QLineEdit;
        QLabel *methodsAddParameterLabel = new QLabel("Type the parameter you want to add. (Example : \"int number\")");
        QPushButton *methodsAddParameterAcceptButton = new QPushButton("Add");
        QPushButton *methodsAddParameterCancelButton = new QPushButton("Cancel");
        QPushButton *methodsAddParameterHistoryButton = new QPushButton("History");

        methodsAddParameterButtonsLayout->addWidget(methodsAddParameterAcceptButton);
        methodsAddParameterButtonsLayout->addWidget(methodsAddParameterCancelButton);
        methodsAddParameterButtonsLayout->addWidget(methodsAddParameterHistoryButton);
        methodsAddParameterLayout->addWidget(methodsAddParameterLabel);
        methodsAddParameterLayout->addWidget(methodsAddParameterLineEdit);
        methodsAddParameterLayout->addLayout(methodsAddParameterButtonsLayout);

        connect(methodsAddParameterAcceptButton, SIGNAL(clicked()), this, SLOT(methodsAddParameterConfirm()));
        connect(methodsAddParameterAcceptButton, SIGNAL(clicked()), methodsAddParameterWin, SLOT(close()));
        connect(methodsAddParameterHistoryButton, SIGNAL(clicked()), this, SLOT(attributesHistory()));
        connect(methodsAddParameterCancelButton, SIGNAL(clicked()), this, SLOT(resetIndex2()));
        connect(methodsAddParameterCancelButton, SIGNAL(clicked()), methodsAddParameterWin, SLOT(close()));

        methodsAddParameterWin->setLayout(methodsAddParameterLayout);
        methodsAddParameterWin->exec();
    }
    methodsAddConfirm();
}

void MainWindow::methodsAddParameterConfirm() {
    tempParameterVector_.append(methodsAddParameterLineEdit->text());
    if (index2_ == methodsAddSpinBox->value()) {
        methodsAddWin->close();
    }
}

void MainWindow::methodsAddConfirm() {
    if (publicRadio->isChecked()) {
        publicMethodsList_.append(new QVector<QString>);
        publicMethodsList_[publicMethodsList_.size() - 1]->append(methodsLineEdit->text());

        if (tempParameterVector_.size() > 0) {

            for (int i = 0; i < tempParameterVector_.size(); i++) {
                publicMethodsList_[publicMethodsList_.size() - 1]->append(tempParameterVector_[i]);
            }
        }

    } else if (protectedRadio->isChecked()) {
        protectedMethodsList_.append(new QVector<QString>);
        protectedMethodsList_[protectedMethodsList_.size() - 1]->append(methodsLineEdit->text());

        if (tempParameterVector_.size() > 0) {

            for (int i = 0; i < tempParameterVector_.size(); i++) {
                protectedMethodsList_[protectedMethodsList_.size() - 1]->append(tempParameterVector_[i]);
            }
        }

    } else {
        privateMethodsList_.append(new QVector<QString>);
        privateMethodsList_[privateMethodsList_.size() - 1]->append(methodsLineEdit->text());

        if (tempParameterVector_.size() > 0) {

            for (int i = 0; i < tempParameterVector_.size(); i++) {
                privateMethodsList_[privateMethodsList_.size() - 1]->append(tempParameterVector_[i]);
            }
        }
    }
    if (methodsAddSpinBox->value() == 0) {
        methodsAddWin->close();

    } else {
        methodsAddParameterWin->close();

    }
}

void MainWindow::attributesAdd() {
    index_ = 0;

    while (index_ < attributesSpinBox->value()) {
        index_++;

        QDialog *attributesAddWin = new QDialog;
        attributesAddWin->setWindowTitle("Add Attribute");

        QVBoxLayout *attributesAddLayout = new QVBoxLayout;
        QHBoxLayout *attributesAddButtonsLayout = new QHBoxLayout;

        QLabel *attributesAddLabel = new QLabel("Type the attribute you want to add. (Example : \"int number\")");
        attributesLineEdit = new QLineEdit;
        QPushButton *attributesAddAcceptButton = new QPushButton("Add");
        QPushButton *attributesAddCancelButton = new QPushButton("Cancel");
        QPushButton *attributesAddHistoryButton = new QPushButton("History");

        attributesAddButtonsLayout->addWidget(attributesAddAcceptButton);
        attributesAddButtonsLayout->addWidget(attributesAddCancelButton);
        attributesAddButtonsLayout->addWidget(attributesAddHistoryButton);
        attributesAddLayout->addWidget(attributesAddLabel);
        attributesAddLayout->addWidget(attributesLineEdit);
        attributesAddLayout->addLayout(attributesAddButtonsLayout);

        connect(attributesAddAcceptButton, SIGNAL(clicked()), this, SLOT(attributesAddConfirm()));
        connect(attributesAddAcceptButton, SIGNAL(clicked()), attributesAddWin, SLOT(close()));
        connect(attributesAddHistoryButton, SIGNAL(clicked()), this, SLOT(attributesHistory()));
        connect(attributesAddCancelButton, SIGNAL(clicked()), this, SLOT(resetIndex()));
        connect(attributesAddCancelButton, SIGNAL(clicked()), attributesAddWin, SLOT(close()));

        attributesAddWin->setLayout(attributesAddLayout);
        attributesAddWin->exec();
    }

}

void MainWindow::attributesEdit() {
    publicAttributesModifyButtonsVector_.clear();
    publicAttributesDeleteButtonsVector_.clear();
    publicMethodsModifyButtonsVector_.clear();
    publicMethodsDeleteButtonsVector_.clear();

    protectedAttributesModifyButtonsVector_.clear();
    protectedAttributesDeleteButtonsVector_.clear();
    protectedMethodsModifyButtonsVector_.clear();
    protectedMethodsDeleteButtonsVector_.clear();

    privateAttributesModifyButtonsVector_.clear();
    privateAttributesDeleteButtonsVector_.clear();
    privateMethodsModifyButtonsVector_.clear();
    privateMethodsDeleteButtonsVector_.clear();

    if (publicAttributesList_.size() + protectedAttributesList_.size() + privateAttributesList_.size() + publicMethodsList_.size() + protectedMethodsList_.size() + privateMethodsList_.size() == 0) {
        QMessageBox::information(this, "Invalid", "No attributes or methods yet.");
        return;

    }
    attributesEditWin = new QDialog;
    attributesEditWin->setWindowTitle("Edit Attributes And Methods");

    QVBoxLayout *attributesEditLayout = new QVBoxLayout;

    if (publicAttributesList_.size() + publicMethodsList_.size() > 0) {
        attributesEditLayout->addWidget(new QLabel("Public :"));
    }
    if (publicAttributesList_.size() > 0) {
        attributesEditLayout->addWidget(new QLabel("Attributes :"));

        for (int i = 0; i < publicAttributesList_.size(); i++) {
            QHBoxLayout *publicAttributesEditHLayout = new QHBoxLayout;
            QPushButton *publicAttributesEditModifyButton = new QPushButton("Modify");
            QPushButton *publicAttributesEditDeleteButton = new QPushButton("Delete");

            publicAttributesEditHLayout->addWidget(new QLabel(publicAttributesList_[i]));
            publicAttributesEditHLayout->addWidget(publicAttributesEditModifyButton);
            publicAttributesEditHLayout->addWidget(publicAttributesEditDeleteButton);

            publicAttributesModifyButtonsVector_.append(publicAttributesEditModifyButton);
            publicAttributesDeleteButtonsVector_.append(publicAttributesEditDeleteButton);

            connect(publicAttributesEditModifyButton, SIGNAL(clicked()), this, SLOT(attributesEditModify()));
            connect(publicAttributesEditDeleteButton, SIGNAL(clicked()), this, SLOT(attributesEditDelete()));

            attributesEditLayout->addLayout(publicAttributesEditHLayout);
        }
    }
    if (publicMethodsList_.size() > 0) {
        attributesEditLayout->addWidget(new QLabel("Methods :"));

        for (int i = 0; i < publicMethodsList_.size(); i++) {
            QString tempString;
            QHBoxLayout *publicMethodsEditHLayout = new QHBoxLayout;
            QPushButton *publicMethodsEditModifyButton = new QPushButton("Modify");
            QPushButton *publicMethodsEditDeleteButton = new QPushButton("Delete");

            tempString += publicMethodsList_[i]->value(0) + "(";

            if (publicMethodsList_[i]->size() > 1) {

                for (int j = 1; j < publicMethodsList_[i]->size(); j++) {
                    tempString += publicMethodsList_[i]->value(j);

                    if (j < publicMethodsList_[i]->size() - 1) {
                        tempString += ", ";

                    }
                }
            }
            tempString += ");";

            publicMethodsEditHLayout->addWidget(new QLabel(tempString));
            publicMethodsEditHLayout->addWidget(publicMethodsEditModifyButton);
            publicMethodsEditHLayout->addWidget(publicMethodsEditDeleteButton);

            publicMethodsModifyButtonsVector_.append(publicMethodsEditModifyButton);
            publicMethodsDeleteButtonsVector_.append(publicMethodsEditDeleteButton);

            connect(publicMethodsEditModifyButton, SIGNAL(clicked()), this, SLOT(methodsEditModify()));
            connect(publicMethodsEditDeleteButton, SIGNAL(clicked()), this, SLOT(attributesEditDelete()));

            attributesEditLayout->addLayout(publicMethodsEditHLayout);
        }
    }

    if (protectedAttributesList_.size() + protectedMethodsList_.size() > 0) {
        attributesEditLayout->addWidget(new QLabel("Protected :"));

    }
    if (protectedAttributesList_.size() > 0) {
        attributesEditLayout->addWidget(new QLabel("Attributes :"));

        for (int i = 0; i < protectedAttributesList_.size(); i++) {
            QHBoxLayout *protectedAttributesEditHLayout = new QHBoxLayout;
            QPushButton *protectedAttributesEditModifyButton = new QPushButton("Modify");
            QPushButton *protectedAttributesEditDeleteButton = new QPushButton("Delete");

            protectedAttributesEditHLayout->addWidget(new QLabel(protectedAttributesList_[i]));
            protectedAttributesEditHLayout->addWidget(protectedAttributesEditModifyButton);
            protectedAttributesEditHLayout->addWidget(protectedAttributesEditDeleteButton);

            protectedAttributesModifyButtonsVector_.append(protectedAttributesEditModifyButton);
            protectedAttributesDeleteButtonsVector_.append(protectedAttributesEditDeleteButton);

            connect(protectedAttributesEditModifyButton, SIGNAL(clicked()), this, SLOT(attributesEditModify()));
            connect(protectedAttributesEditDeleteButton, SIGNAL(clicked()), this, SLOT(attributesEditDelete()));

            attributesEditLayout->addLayout(protectedAttributesEditHLayout);
        }
    }
    if (protectedMethodsList_.size() > 0) {
        attributesEditLayout->addWidget(new QLabel("Methods :"));

        for (int i = 0; i < protectedMethodsList_.size(); i++) {
            QString tempString;
            QHBoxLayout *protectedMethodsEditHLayout = new QHBoxLayout;
            QPushButton *protectedMethodsEditModifyButton = new QPushButton("Modify");
            QPushButton *protectedMethodsEditDeleteButton = new QPushButton("Delete");

            tempString += protectedMethodsList_[i]->value(0) + "(";

            if (protectedMethodsList_[i]->size() > 1) {

                for (int j = 1; j < protectedMethodsList_[i]->size(); j++) {
                    tempString += protectedMethodsList_[i]->value(j);

                    if (j < protectedMethodsList_[i]->size() - 1) {
                        tempString += ", ";

                    }
                }
            }
            tempString += ");";

            protectedMethodsEditHLayout->addWidget(new QLabel(tempString));
            protectedMethodsEditHLayout->addWidget(protectedMethodsEditModifyButton);
            protectedMethodsEditHLayout->addWidget(protectedMethodsEditDeleteButton);

            protectedMethodsModifyButtonsVector_.append(protectedMethodsEditModifyButton);
            protectedMethodsDeleteButtonsVector_.append(protectedMethodsEditDeleteButton);

            connect(protectedMethodsEditModifyButton, SIGNAL(clicked()), this, SLOT(methodsEditModify()));
            connect(protectedMethodsEditDeleteButton, SIGNAL(clicked()), this, SLOT(attributesEditDelete()));

            attributesEditLayout->addLayout(protectedMethodsEditHLayout);
        }
    }
    if (privateAttributesList_.size() + privateMethodsList_.size() > 0) {
        attributesEditLayout->addWidget(new QLabel("Private :"));

    }
    if (privateAttributesList_.size() > 0) {
        attributesEditLayout->addWidget(new QLabel("Attributes :"));

        for (int i = 0; i < privateAttributesList_.size(); i++) {
            QHBoxLayout *privateAttributesEditHLayout = new QHBoxLayout;
            QPushButton *privateAttributesEditModifyButton = new QPushButton("Modify");
            QPushButton *privateAttributesEditDeleteButton = new QPushButton("Delete");

            privateAttributesEditHLayout->addWidget(new QLabel(privateAttributesList_[i]));
            privateAttributesEditHLayout->addWidget(privateAttributesEditModifyButton);
            privateAttributesEditHLayout->addWidget(privateAttributesEditDeleteButton);

            privateAttributesModifyButtonsVector_.append(privateAttributesEditModifyButton);
            privateAttributesDeleteButtonsVector_.append(privateAttributesEditDeleteButton);

            connect(privateAttributesEditModifyButton, SIGNAL(clicked()), this, SLOT(attributesEditModify()));
            connect(privateAttributesEditDeleteButton, SIGNAL(clicked()), this, SLOT(attributesEditDelete()));

            attributesEditLayout->addLayout(privateAttributesEditHLayout);
        }
    }
    if (privateMethodsList_.size() > 0) {
        attributesEditLayout->addWidget(new QLabel("Methods :"));

        for (int i = 0; i < privateMethodsList_.size(); i++) {
            QString tempString;
            QHBoxLayout *privateMethodsEditHLayout = new QHBoxLayout;
            QPushButton *privateMethodsEditModifyButton = new QPushButton("Modify");
            QPushButton *privateMethodsEditDeleteButton = new QPushButton("Delete");

            tempString += privateMethodsList_[i]->value(0) + "(";

            if (privateMethodsList_[i]->size() > 1) {

                for (int j = 1; j < privateMethodsList_[i]->size(); j++) {
                    tempString += privateMethodsList_[i]->value(j);

                    if (j < privateMethodsList_[i]->size() - 1) {
                        tempString += ", ";

                    }
                }
            }
            tempString += ");";

            privateMethodsEditHLayout->addWidget(new QLabel(tempString));
            privateMethodsEditHLayout->addWidget(privateMethodsEditModifyButton);
            privateMethodsEditHLayout->addWidget(privateMethodsEditDeleteButton);

            privateMethodsModifyButtonsVector_.append(privateMethodsEditModifyButton);
            privateMethodsDeleteButtonsVector_.append(privateMethodsEditDeleteButton);

            connect(privateMethodsEditModifyButton, SIGNAL(clicked()), this, SLOT(methodsEditModify()));
            connect(privateMethodsEditDeleteButton, SIGNAL(clicked()), this, SLOT(attributesEditDelete()));

            attributesEditLayout->addLayout(privateMethodsEditHLayout);
        }
    }
    QPushButton *editAttributesReturnButton = new QPushButton("Return");
    connect(editAttributesReturnButton, SIGNAL(clicked()), attributesEditWin, SLOT(close()));
    
    attributesEditLayout->addWidget(editAttributesReturnButton);
    attributesEditWin->setLayout(attributesEditLayout);

    attributesEditWin->exec();
}

void MainWindow::attributesEditModify() {
    QObject *obj = sender();

    for (int i = 0; i < publicAttributesModifyButtonsVector_.size(); i++) {
        if (publicAttributesModifyButtonsVector_[i] == obj) {
            index_ = i;
            index2_ = 0;
        }
    }
    for (int i = 0; i < protectedAttributesModifyButtonsVector_.size(); i++) {
        if (protectedAttributesModifyButtonsVector_[i] == obj) {
            index_ = i;
            index2_ = 0;
        }
    }
    for (int i = 0; i < privateAttributesModifyButtonsVector_.size(); i++) {
        if (privateAttributesModifyButtonsVector_[i] == obj) {
            index_ = i;
            index2_ = 0;
        }
    }

    QDialog *attributesEditModifyWin = new QDialog;
    attributesEditModifyWin->setWindowTitle("Edit Attribute");
    attributesEditLineEdit = new QLineEdit;

    QVBoxLayout *attributesEditModifyLayout = new QVBoxLayout;
    QHBoxLayout *attributesEditModifyButtonsLayout = new QHBoxLayout;
    QPushButton *attributesEditModifyAcceptButton = new QPushButton("Modify");
    QPushButton *attributesEditModifyCancelButton = new QPushButton("Cancel");

    switch(index2_) {
        case 0: attributesEditLineEdit->setText(publicAttributesList_[index_]); break;
        case 1: attributesEditLineEdit->setText(protectedAttributesList_[index_]); break;
        case 2: attributesEditLineEdit->setText(privateAttributesList_[index_]); break;
    }
    connect(attributesEditModifyAcceptButton, SIGNAL(clicked()), this, SLOT(attributesEditModifyConfirm()));
    connect(attributesEditModifyAcceptButton, SIGNAL(clicked()), attributesEditModifyWin, SLOT(close()));
    connect(attributesEditModifyCancelButton, SIGNAL(clicked()), attributesEditModifyWin, SLOT(close()));

    attributesEditModifyButtonsLayout->addWidget(attributesEditModifyAcceptButton);
    attributesEditModifyButtonsLayout->addWidget(attributesEditModifyCancelButton);

    attributesEditModifyLayout->addWidget(attributesEditLineEdit);
    attributesEditModifyLayout->addLayout(attributesEditModifyButtonsLayout);

    attributesEditModifyWin->setLayout(attributesEditModifyLayout);
    attributesEditModifyWin->exec();
}

void MainWindow::attributesEditModifyConfirm() {
    switch(index2_) {
        case 0: publicAttributesList_[index_] = attributesEditLineEdit->text(); break;
        case 1: protectedAttributesList_[index_] = attributesEditLineEdit->text(); break;
        case 2: privateAttributesList_[index_] = attributesEditLineEdit->text(); break;
    }
    attributesEditWin->close();
    attributesEdit();
}

void MainWindow::attributesEditDelete() {
    QObject *obj = sender();

    for (int i = 0; i < publicAttributesList_.size(); i++) {
        if (publicAttributesDeleteButtonsVector_[i] == obj) {
            publicAttributesList_.remove(i);
            
        }
    }
    for (int i = 0; i < protectedAttributesList_.size(); i++) {
        if (protectedAttributesDeleteButtonsVector_[i] == obj) {
            protectedAttributesList_.remove(i);

        }
    }
    for (int i = 0; i < privateAttributesList_.size(); i++) {
        if (privateAttributesDeleteButtonsVector_[i] == obj) {
            privateAttributesList_.remove(i);

        }
    }
    for (int i = 0; i < publicMethodsList_.size(); i++) {
        if (publicMethodsDeleteButtonsVector_[i] == obj) {
            publicMethodsList_.remove(i);

        }
    }
    for (int i = 0; i < protectedMethodsList_.size(); i++) {
        if (protectedMethodsDeleteButtonsVector_[i] == obj) {
            protectedMethodsList_.remove(i);

        }
    }
    for (int i = 0; i < privateMethodsList_.size(); i++) {
        if (privateMethodsDeleteButtonsVector_[i] == obj) {
            privateMethodsList_.remove(i);

        }
    }
    attributesEditWin->close();
    attributesEdit();
}

void MainWindow::methodsEditModify() {
    QObject *obj = sender();

    for (int i = 0; i < publicMethodsModifyButtonsVector_.size(); i++) {
        if (publicMethodsModifyButtonsVector_[i] == obj) {
            index_ = i;
            index2_ = 0;
        }
    }
    for (int i = 0; i < protectedMethodsModifyButtonsVector_.size(); i++) {
        if (protectedMethodsModifyButtonsVector_[i] == obj) {
            index_ = i;
            index2_ = 1;
        }
    }
    for (int i = 0; i < privateMethodsModifyButtonsVector_.size(); i++) {
        if (privateMethodsModifyButtonsVector_[i] == obj) {
            index_ = i;
            index2_ = 2;
        }
    }

    methodsEditModifyWin = new QDialog;
    methodsEditModifyWin->setWindowTitle("Edit Method");
    methodsEditLineEdit = new QLineEdit;

    QHBoxLayout *methodsEditModifyButtonsLayout = new QHBoxLayout;
    QVBoxLayout *methodsEditModifyLayout = new QVBoxLayout;

    QTextEdit *methodsEditParameterList = new QTextEdit;
    QTextDocument *methodsEditParameterDocument = new QTextDocument;
    QString tempString("Parameters : \n\n");
    methodsEditParameterList->setReadOnly(true);

    QPushButton *methodsEditModifyAcceptButton = new QPushButton("Modify");
    QPushButton *methodsEditModifyParamButton = new QPushButton("Modify Parameters");
    QPushButton *methodsEditModifyAddParamButton = new QPushButton("Add Parameter");
    QPushButton *methodsEditModifyCancelButton = new QPushButton("Cancel");

    switch(index2_) {
        case 0:
            methodsEditLineEdit->setText(publicMethodsList_[index_]->value(0));
            if (publicMethodsList_[index_]->size() > 1) {
                for (int i = 1; i < publicMethodsList_[index_]->size(); i++) {
                    tempString += publicMethodsList_[index_]->value(i) + "\n";
                }
            }
            break;

        case 1:
            methodsEditLineEdit->setText(protectedMethodsList_[index_]->value(0));
            if (protectedMethodsList_[index_]->size() > 1) {
                for (int i = 1; i < protectedMethodsList_[index_]->size(); i++) {
                    tempString += protectedMethodsList_[index_]->value(i) + "\n";
                }
            }
            break;

        case 2:
            methodsEditLineEdit->setText(privateMethodsList_[index_]->value(0));
            if (privateMethodsList_[index_]->size() > 1) {
                for (int i = 1; i < privateMethodsList_[index_]->size(); i++) {
                    tempString += privateMethodsList_[index_]->value(i) + "\n";
                }
            }
            break;
    }
    methodsEditParameterDocument->setPlainText(tempString);
    methodsEditParameterList->setDocument(methodsEditParameterDocument);

    methodsEditModifyButtonsLayout->addWidget(methodsEditModifyAcceptButton);
    methodsEditModifyButtonsLayout->addWidget(methodsEditModifyParamButton);
    methodsEditModifyButtonsLayout->addWidget(methodsEditModifyAddParamButton);
    methodsEditModifyButtonsLayout->addWidget(methodsEditModifyCancelButton);

    connect(methodsEditModifyAcceptButton, SIGNAL(clicked()), this, SLOT(methodsEditModifyConfirm()));
    connect(methodsEditModifyAcceptButton, SIGNAL(clicked()), methodsEditModifyWin, SLOT(close()));
    connect(methodsEditModifyParamButton, SIGNAL(clicked()), this, SLOT(methodsEditModifyParam()));
    connect(methodsEditModifyCancelButton, SIGNAL(clicked()), methodsEditModifyWin, SLOT(close()));
    connect(methodsEditModifyAddParamButton, SIGNAL(clicked()), this, SLOT(methodsAddParam()));

    methodsEditModifyLayout->addWidget(methodsEditLineEdit);
    methodsEditModifyLayout->addWidget(methodsEditParameterList);
    methodsEditModifyLayout->addLayout(methodsEditModifyButtonsLayout);

    methodsEditModifyWin->setLayout(methodsEditModifyLayout);
    methodsEditModifyWin->exec();
}

void MainWindow::methodsEditModifyConfirm() {

}

void MainWindow::methodsAddParam() {
    QDialog *methodsAddParamWin = new QDialog;
    methodsAddParamWin->setWindowTitle("Add Parameter");

    QHBoxLayout *methodsAddParamButtonsLayout = new QHBoxLayout;
    QVBoxLayout *methodsAddParamLayout = new QVBoxLayout;

    QPushButton *methodsAddParamAcceptButton = new QPushButton("Add");
    QPushButton *methodsAddParamCancelButton = new QPushButton("Cancel");
    tempLineEdit_ = new QLineEdit;
    QLabel *methodsAddParamLabel = new QLabel("Type the parameter you want to add. (Example : Type \"int test\")");

    methodsAddParamButtonsLayout->addWidget(methodsAddParamAcceptButton);
    methodsAddParamButtonsLayout->addWidget(methodsAddParamCancelButton);

    connect(methodsAddParamAcceptButton, SIGNAL(clicked()), this, SLOT(methodsAddParamConfirm()));
    connect(methodsAddParamAcceptButton, SIGNAL(clicked()), methodsAddParamWin, SLOT(close()));
    connect(methodsAddParamCancelButton, SIGNAL(clicked()), methodsAddParamWin, SLOT(close()));

    methodsAddParamLayout->addWidget(methodsAddParamLabel);
    methodsAddParamLayout->addWidget(tempLineEdit_);
    methodsAddParamLayout->addLayout(methodsAddParamButtonsLayout);

    methodsAddParamWin->setLayout(methodsAddParamLayout);
    methodsAddParamWin->exec();
}

void MainWindow::methodsAddParamConfirm() {
    switch(index2_) {
        case 0:
            publicMethodsList_[index_]->append(tempLineEdit_->text());
            methodsEditModifyWin->close();
            methodsEditModify();
            break;

        case 1:
            protectedMethodsList_[index_]->append(tempLineEdit_->text());
            methodsEditModifyWin->close();
            methodsEditModify();
            break;

        case 2:
            privateMethodsList_[index_]->append(tempLineEdit_->text());
            methodsEditModifyWin->close();
            methodsEditModify();
            break;
    }
}

void MainWindow::methodsEditModifyParam() {
    switch(index2_) {
        case 0:
            if (publicMethodsList_[index_]->size() <= 1) {
                QMessageBox::information(methodsEditModifyWin, "No Parameters", "You don't have any parameters !");
                return;
            }
            break;

        case 1:
            if (protectedMethodsList_[index_]->size() <= 1) {
                QMessageBox::information(methodsEditModifyWin, "No Parameters", "You don't have any parameters !");
                return;
            }
            break;

        case 2:
            if (privateMethodsList_[index_]->size() <= 1) {
                QMessageBox::information(methodsEditModifyWin, "No Parameters", "You don't have any parameters !");
                return;
            }
            break;
    }
    methodsEditModifyParamWin = new QDialog;
    methodsEditModifyParamWin->setWindowTitle("Edit Parameters");

    QHBoxLayout *methodsEditModifyParamButtonsLayout = new QHBoxLayout;
    QVBoxLayout *methodsEditModifyParamLayout = new QVBoxLayout;


    switch(index2_) {
        case 0:
            for (int i = 1; i < publicMethodsList_[index_]->size(); i++) {
                QLineEdit *tempLineEdit = new QLineEdit(publicMethodsList_[index_]->value(i));
                methodsEditModifyChangesVector_.append(tempLineEdit);
                methodsEditModifyParamLayout->addWidget(tempLineEdit);
            }
            break;

        case 1:
            for (int i = 1; i < protectedMethodsList_[index_]->size(); i++) {
                QLineEdit *tempLineEdit = new QLineEdit(protectedMethodsList_[index_]->value(i));
                methodsEditModifyChangesVector_.append(tempLineEdit);
                methodsEditModifyParamLayout->addWidget(tempLineEdit);
            }
            break;

        case 2:
            for (int i = 1; i < privateMethodsList_[index_]->size(); i++) {
                QLineEdit *tempLineEdit = new QLineEdit(privateMethodsList_[index_]->value(i));
                methodsEditModifyChangesVector_.append(tempLineEdit);
                methodsEditModifyParamLayout->addWidget(tempLineEdit);
            }
            break;
    }

    QPushButton *methodsEditModifyParamAcceptButton = new QPushButton("Modify");
    QPushButton *methodsEditModifyParamCancelButton = new QPushButton("Cancel");

    methodsEditModifyParamButtonsLayout->addWidget(methodsEditModifyParamAcceptButton);
    methodsEditModifyParamButtonsLayout->addWidget(methodsEditModifyParamCancelButton);

    connect(methodsEditModifyParamAcceptButton, SIGNAL(clicked()), this, SLOT(methodsEditModifyParamConfirm()));
    connect(methodsEditModifyParamCancelButton, SIGNAL(clicked()), methodsEditModifyParamWin, SLOT(close()));

    methodsEditModifyParamLayout->addLayout(methodsEditModifyParamButtonsLayout);
    methodsEditModifyParamWin->setLayout(methodsEditModifyParamLayout);
    methodsEditModifyParamWin->exec();
}

void MainWindow::methodsEditModifyParamConfirm() {
    switch(index2_) {
        case 0:
            for (int i = 1; i < publicMethodsList_[index_]->size(); i++) {
                publicMethodsList_[index_]->replace(i, methodsEditModifyChangesVector_[i - 1]->text());
                if (methodsEditModifyChangesVector_[i - 1]->text().isEmpty()) {
                    publicMethodsList_[index_]->replace(i, "");
                }
            }
            for (int i = 1; i < publicMethodsList_[index_]->size(); i++) {
                if (publicMethodsList_[index_]->value(i).isEmpty()) {
                    publicMethodsList_[index_]->remove(i);
                    i--;

                }
            }
            break;

        case 1:
            for (int i = 1; i < protectedMethodsList_[index_]->size(); i++) {
                protectedMethodsList_[index_]->replace(i, methodsEditModifyChangesVector_[i - 1]->text());
            }
            for (int i = 1; i < protectedMethodsList_[index_]->size(); i++) {
                if (protectedMethodsList_[index_]->value(i).isEmpty()) {
                    protectedMethodsList_[index_]->remove(i);
                    i--;

                }
            }
            break;

        case 2:
            for (int i = 1; i < privateMethodsList_[index_]->size(); i++) {
                privateMethodsList_[index_]->replace(i, methodsEditModifyChangesVector_[i - 1]->text());
            }
            for (int i = 1; i < privateMethodsList_[index_]->size(); i++) {
                if (privateMethodsList_[index_]->value(i).isEmpty()) {
                    privateMethodsList_[index_]->remove(i);
                    i--;

                }
            }
            break;
    }
    methodsEditModifyParamWin->close();
    methodsEditModifyWin->close();
    methodsEditModify();
}

void MainWindow::attributesHistory() {
    if (publicAttributesList_.size() + protectedAttributesList_.size() + privateAttributesList_.size() + 
        publicMethodsList_.size() + protectedMethodsList_.size() + privateMethodsList_.size() == 0) {

        QMessageBox::information(this, "Invalid", "No attributes or methods yet.");
        return;

    }
    QDialog *attributesHistoryWin = new QDialog;
    attributesHistoryWin->setWindowTitle("Attributes and Methods History");
    QVBoxLayout *attributesHistoryMainLayout = new QVBoxLayout;

    if (publicAttributesList_.size() > 0 || publicMethodsList_.size() > 0) {
        QGroupBox *publicGroupBox = new QGroupBox("Public :");
        QVBoxLayout *publicVBox = new QVBoxLayout;

        if (publicAttributesList_.size() > 0) {
            publicVBox->addWidget(new QLabel(""));
            publicVBox->addWidget(new QLabel("Attributes :"));

            for (int i = 0; i < publicAttributesList_.size(); i++) {
                publicVBox->addWidget(new QLabel(QString::number(i + 1) + " - " + publicAttributesList_[i]));
            }

            publicVBox->addWidget(new QLabel(""));
        }

        if (publicMethodsList_.size() > 0) {
            publicVBox->addWidget(new QLabel(""));
            publicVBox->addWidget(new QLabel("Methods :"));

            for (int i = 0; i < publicMethodsList_.size(); i++) {
                QString tempString("");

                tempString += QString::number(i + 1) + " - " +  publicMethodsList_[i]->value(0) + "(";
                if (publicMethodsList_[i]->size() > 1) {

                    for (int j = 1; j < publicMethodsList_[i]->size(); j++) {
                        tempString += publicMethodsList_[i]->value(j);
                        if (j < publicMethodsList_[i]->size() - 1) {
                            tempString += ", ";
                        }
                    }
                }
                tempString += ");";
                publicVBox->addWidget(new QLabel(tempString));
            }
        }

        publicGroupBox->setLayout(publicVBox);
        attributesHistoryMainLayout->addWidget(publicGroupBox);
    }

    if (protectedAttributesList_.size() > 0 || protectedMethodsList_.size() > 0) {
        QGroupBox *protectedGroupBox = new QGroupBox("Public :");
        QVBoxLayout *protectedVBox = new QVBoxLayout;

        if (protectedAttributesList_.size() > 0) {
            protectedVBox->addWidget(new QLabel(""));
            protectedVBox->addWidget(new QLabel("Attributes :"));

            for (int i = 0; i < protectedAttributesList_.size(); i++) {
                protectedVBox->addWidget(new QLabel(QString::number(i + 1) + " - " + protectedAttributesList_[i]));
            }

            protectedVBox->addWidget(new QLabel(""));
        }

        if (protectedMethodsList_.size() > 0) {
            protectedVBox->addWidget(new QLabel(""));
            protectedVBox->addWidget(new QLabel("Methods :"));

            for (int i = 0; i < protectedMethodsList_.size(); i++) {
                QString tempString("");

                tempString += QString::number(i + 1) + " - " + protectedMethodsList_[i]->value(0) + "(";
                if (protectedMethodsList_[i]->size() > 1) {

                    for (int j = 1; j < protectedMethodsList_[i]->size(); j++) {
                        tempString += protectedMethodsList_[i]->value(j);
                        if (j < protectedMethodsList_[i]->size() - 1) {
                            tempString += ", ";
                        }
                    }
                }
                tempString += ");";
                protectedVBox->addWidget(new QLabel(tempString));
            }
        }

        protectedGroupBox->setLayout(protectedVBox);
        attributesHistoryMainLayout->addWidget(protectedGroupBox);
    }

    if (privateAttributesList_.size() > 0 || privateMethodsList_.size() > 0) {
        QGroupBox *privateGroupBox = new QGroupBox("Private :");
        QVBoxLayout *privateVBox = new QVBoxLayout;

        if (privateAttributesList_.size() > 0) {
            privateVBox->addWidget(new QLabel(""));
            privateVBox->addWidget(new QLabel("Attributes :"));

            for (int i = 0; i < privateAttributesList_.size(); i++) {
                privateVBox->addWidget(new QLabel(QString::number(i + 1) + " - " + privateAttributesList_[i]));
            }

            privateVBox->addWidget(new QLabel(""));
        }

        if (privateMethodsList_.size() > 0) {
            privateVBox->addWidget(new QLabel(""));
            privateVBox->addWidget(new QLabel("Methods :"));

            for (int i = 0; i < privateMethodsList_.size(); i++) {
                QString tempString("");

                tempString += QString::number(i + 1) + " - " + privateMethodsList_[i]->value(0) + "(";
                if (privateMethodsList_[i]->size() > 1) {

                    for (int j = 1; j < privateMethodsList_[i]->size(); j++) {
                        tempString += privateMethodsList_[i]->value(j);
                        if (j < privateMethodsList_[i]->size() - 1) {
                            tempString += ", ";
                        }
                    }
                }
                tempString += ");";
                privateVBox->addWidget(new QLabel(tempString));
            }
        }

        privateGroupBox->setLayout(privateVBox);
        attributesHistoryMainLayout->addWidget(privateGroupBox);
    }

    QPushButton *attributesHistoryReturnButton = new QPushButton("Back");
    attributesHistoryMainLayout->addWidget(attributesHistoryReturnButton);
    attributesHistoryWin->setLayout(attributesHistoryMainLayout);

    connect(attributesHistoryReturnButton, SIGNAL(clicked()), attributesHistoryWin, SLOT(close()));

    attributesHistoryWin->exec();
}

void MainWindow::attributesAddConfirm() {
    if (publicRadio->isChecked()) {
        publicAttributesList_.append(attributesLineEdit->text());

    } else if (protectedRadio->isChecked()) {
        protectedAttributesList_.append(attributesLineEdit->text());

    } else {
        privateAttributesList_.append(attributesLineEdit->text());

    }
}
