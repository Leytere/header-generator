#include "GeneratedCodeWindow.h"

GeneratedCodeWindow::GeneratedCodeWindow(QString code, QString codeCpp, QString name): code_(code), codeCpp_(codeCpp), name_(name) {
    QTabWidget *mainTabWin = new QTabWidget;
    mainTabWin->setWindowTitle("Generated Code");

    //Header Tab
    QDialog *headerCodeWin = new QDialog;

    QVBoxLayout *layout = new QVBoxLayout;
    QHBoxLayout *buttonsLayout = new QHBoxLayout;

    QPushButton *button = new QPushButton("Close");
    QPushButton *saveButton = new QPushButton("Save Files");
    QPushButton *clipboardButton = new QPushButton("Copy to Clipboard");
    text = new QTextEdit;

    buttonsLayout->addWidget(saveButton);
    buttonsLayout->addWidget(clipboardButton);
    buttonsLayout->addWidget(button);

    text->setPlainText(code);
    text->setReadOnly(true);
    text->setFont(QFont("Courier"));
    text->setLineWrapMode(QTextEdit::NoWrap);

    layout->addWidget(text);
    layout->addLayout(buttonsLayout);

    headerCodeWin->setLayout(layout);

    //Cpp Tab
    QDialog *cppCodeWin = new QDialog;

    QHBoxLayout *cppButtonsLayout = new QHBoxLayout;
    QVBoxLayout *cppLayout = new QVBoxLayout;

    QPushButton *cppButton = new QPushButton("Close");
    QPushButton *cppSaveButton = new QPushButton("Save Files");
    QPushButton *cppClipboardButton = new QPushButton("Copy to Clipboard");
    cppText = new QTextEdit;

    cppButtonsLayout->addWidget(cppSaveButton);
    cppButtonsLayout->addWidget(cppClipboardButton);
    cppButtonsLayout->addWidget(cppButton);

    cppText->setPlainText(codeCpp);
    cppText->setReadOnly(true);
    cppText->setFont(QFont("Courier"));
    cppText->setLineWrapMode(QTextEdit::NoWrap);

    cppLayout->addWidget(cppText);
    cppLayout->addLayout(cppButtonsLayout);

    cppCodeWin->setLayout(cppLayout);

    mainTabWin->addTab(headerCodeWin, ".h");
    mainTabWin->addTab(cppCodeWin, ".cpp");
    mainTabWin->show();
    
    QWidget::connect(button, SIGNAL(clicked()), mainTabWin, SLOT(close()));
    QWidget::connect(cppButton, SIGNAL(clicked()), mainTabWin, SLOT(close()));

    QWidget::connect(clipboardButton, SIGNAL(clicked()), this, SLOT(copyHClipboard()));
    QWidget::connect(cppClipboardButton, SIGNAL(clicked()), this, SLOT(copyCClipboard()));

    QWidget::connect(saveButton, SIGNAL(clicked()), this, SLOT(openSave()));
    QWidget::connect(cppSaveButton, SIGNAL(clicked()), this, SLOT(openSave()));
}

void GeneratedCodeWindow::copyHClipboard() {
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(text->toPlainText());
    text->selectAll();
}

void GeneratedCodeWindow::copyCClipboard() {
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(cppText->toPlainText());
    cppText->selectAll();
}

void GeneratedCodeWindow::openSave() {
    QString folder(QFileDialog::getExistingDirectory(this, "Choose a Folder", "/", QFileDialog::ShowDirsOnly));
    QFile hFile(folder + "/" + name_ + ".h");

    if (hFile.open(QIODevice::ReadWrite)) {
        QTextStream streamH(&hFile);
        streamH << code_;
    }
    QFile cppFile(folder + "/" + name_ + ".cpp");

    if (cppFile.open(QIODevice::ReadWrite)) {
        QTextStream streamCpp(&cppFile);
        streamCpp << codeCpp_;
    }

    QMessageBox::information(this, "Saved", "Files Saved in " + folder);
}
