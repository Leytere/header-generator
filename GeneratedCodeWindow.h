#ifndef DEF_GENERATEDCODEWINDOW
#define DEF_GENERATEDCODEWINDOW

#include <QApplication>
#include <QString>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QWidget>
#include <QTabWidget>
#include <QClipboard>
#include <QFileDialog>
#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QMessageBox>

class GeneratedCodeWindow: public QTabWidget {
    Q_OBJECT
    public:
        GeneratedCodeWindow(QString code, QString codeCpp, QString name);

    private slots:
        void copyCClipboard();
        void copyHClipboard();
        void openSave();

    private:
        QString name_;
        QString code_;
        QString codeCpp_;
        
        QTextEdit *text;
        QTextEdit *cppText;
};

#endif
