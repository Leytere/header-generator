#ifndef DEF_MAINWINDOW
#define DEF_MAINWINDOW

#include <QLineEdit>
#include <QCheckBox>
#include <QGroupBox>
#include <QDateEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QLabel>
#include <QApplication>
#include <QMessageBox>
#include <QDate>
#include <QDateTime>
#include <QSpinBox>
#include <QRadioButton>
#include <QButtonGroup>

class MainWindow: public QWidget {
    Q_OBJECT

    public:
        MainWindow();

    private slots:
        void generateCode();
        void defHeaderDis();
        void changeHead();

        void displayIncludesAdd();
        void resetIndex();
        void resetIndex2();
        void acceptInclude();
        void includesHistory();
        void editIncludes();
        void deleteInclude();
        void modifyInclude();
        void acceptIncludeModification();

        void attribsAndMethodsCheck();
        void attributesAdd();

        void methodsAdd();
        void methodsAddConfirm();
        void methodsAddParameters();
        void methodsAddParameterConfirm();

        void attributesEdit();
        void attributesEditModify();
        void attributesEditModifyConfirm();

        void methodsEditModify();
        void methodsEditModifyConfirm();
        void methodsEditModifyParam();
        void methodsEditModifyParamConfirm();
        void methodsAddParam();
        void methodsAddParamConfirm();

        void attributesEditDelete();
        void attributesHistory();
        void attributesAddConfirm();

    private:
        QLineEdit *name;
        QLineEdit *parentClass;
        QCheckBox *optionsCheck1;
        QCheckBox *optionsCheck2;
        QCheckBox *optionsCheck3;
        QCheckBox *optionsCheck4;
        QGroupBox *commentGroupBox;
        QLineEdit *author;
        QLineEdit *defHeaderLine;
        QDateEdit *creaDate;
        QTextEdit *classRole;
        QPushButton *generateButton;
        QPushButton *exitButton;
        QSpinBox *includesNumber;
        QLineEdit *includesLineEdit;

        QDialog *addIncludesWin;
        QDialog *editIncludesWin;
        QDialog *modifyIncludeWin;
        QLineEdit *modifyIncludeLineEdit;

        QRadioButton *publicRadio;
        QRadioButton *protectedRadio;
        QRadioButton *privateRadio;
        QRadioButton *attributesRadio;
        QRadioButton *methodsRadio;
        QSpinBox *attributesSpinBox;

        QLineEdit *attributesLineEdit;
        QLineEdit *methodsLineEdit;
        QLineEdit *methodsAddParameterLineEdit;
        QSpinBox *methodsAddSpinBox;
        QDialog *methodsAddWin;
        QDialog *methodsAddParameterWin;

        int index_;
        int index2_;
        QLineEdit *tempLineEdit_;
        QVector<QString> tempParameterVector_;

        QString headerText_;
        QVector<QString> includesList_;
        QVector<QString> publicAttributesList_;
        QVector<QString> protectedAttributesList_;
        QVector<QString> privateAttributesList_;
        QVector< QVector<QString>* > publicMethodsList_;
        QVector< QVector<QString>* > protectedMethodsList_;
        QVector< QVector<QString>* > privateMethodsList_;

        QDialog *attributesEditWin;
        QDialog *methodsEditModifyWin;
        QDialog *methodsEditModifyParamWin;
        QLineEdit *attributesEditLineEdit;
        QLineEdit *methodsEditLineEdit;
        QVector<QPushButton*> buttonsVector_;
        QVector<QPushButton*> modifyButtonsVector_;
        QVector<QLineEdit*> methodsEditModifyChangesVector_;

        QVector<QPushButton*> publicAttributesDeleteButtonsVector_;
        QVector<QPushButton*> publicAttributesModifyButtonsVector_;
        QVector<QPushButton*> protectedAttributesDeleteButtonsVector_;
        QVector<QPushButton*> protectedAttributesModifyButtonsVector_;
        QVector<QPushButton*> privateAttributesDeleteButtonsVector_;
        QVector<QPushButton*> privateAttributesModifyButtonsVector_;

        QVector<QPushButton*> publicMethodsModifyButtonsVector_;
        QVector<QPushButton*> publicMethodsDeleteButtonsVector_;
        QVector<QPushButton*> protectedMethodsModifyButtonsVector_;
        QVector<QPushButton*> protectedMethodsDeleteButtonsVector_;
        QVector<QPushButton*> privateMethodsModifyButtonsVector_;
        QVector<QPushButton*> privateMethodsDeleteButtonsVector_;
};

#endif
